import React from 'react';

import {useRouter} from "next/router";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";

import classes from '../about/about-pages.module.css';
import BannerImage from '/public/assets/images/classroom.jpg';
import Layout from "../../components/layouts/layout";
import {extractActiveLinkText} from "../../helpers/extract-links";
import {navMenu} from "../../misc/data";
import FooterImage from "../../public/assets/images/img_13-sm.png";
import FirstTermCalendar from "../../components/calendar/terms-schedule";

const AcademicCalendarPage = () => {
    const router = useRouter();

    return (
        <Layout
            activeLinkText={extractActiveLinkText(router.pathname, navMenu.links.academics)}
            bannerImage={BannerImage}
            footerImage={FooterImage}
            navLinks={navMenu.links.academics}
            parentLinkText={'Academics'}
        >
            <Tabs defaultActiveKey={'first'}>
                <Tab eventKey={'first'} title={'First Term'}>
                    <div className={'mt-4'}>
                        <h3 className={`${classes['body-sub-heading']} mb-3 text-uppercase`}>Important Dates</h3>
                        <FirstTermCalendar/>
                    </div>
                </Tab>
                {/*<Tab eventKey={'second'} title={'Second Term'}>*/}
                {/*    <div className={'mt-4'}>*/}
                {/*        <h3 className={`${classes['body-sub-heading']} mb-3 text-uppercase`}>Important Dates</h3>*/}
                {/*        <SecondTermCalendar/>*/}
                {/*    </div>*/}
                {/*</Tab>*/}
                {/*<Tab eventKey={'third'} title={'Third Term'}>*/}
                {/*    <div className={'mt-4'}>*/}
                {/*        <h3 className={`${classes['body-sub-heading']} mb-3 text-uppercase`}>Important Dates</h3>*/}
                {/*        <ThirdTermCalendar/>*/}
                {/*    </div>*/}
                {/*</Tab>*/}
            </Tabs>
        </Layout>
    );
};

export default AcademicCalendarPage;
