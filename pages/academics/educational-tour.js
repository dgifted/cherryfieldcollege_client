import React from 'react';

import {useRouter} from "next/router";

import classes from '../about/about-pages.module.css';
import BannerImage from '/public/assets/images/school_buses.png';
import Layout from "../../components/layouts/layout";
import {extractActiveLinkText} from "../../helpers/extract-links";
import {navMenu} from "../../misc/data";
import FooterImage from "../../public/assets/images/img_13-sm.png";

const EducationalTourPage = () => {
    const router = useRouter();

    return (
        <Layout
            activeLinkText={extractActiveLinkText(router.pathname, navMenu.links.academics)}
            bannerImage={BannerImage}
            footerImage={FooterImage}
            navLinks={navMenu.links.academics}
            parentLinkText={'Academics'}
        >
            <p className={classes['intro-text']}>
                Every year our children embark on exciting and fascinating trip to the UK to experience first class
                taught academic courses with a Football Academy experience, superb accommodation/environment and World
                Class British hospitality in a UK college of excellence.
            </p>

            <p className={classes['regular-text']}>
                The College is a first class college in the UK with a ranking by the British Accreditation Council. It
                provides the summer school students from JSS 1 – SS 2 (i.e. Ages 9 – 16yrs) a robust package of
                interactive lessons which combines English language studies for specific purpose, study skills and
                social skills in the morning with leisure, cultural and excursion activities in the afternoon and
                evening.
            </p>

            <p className={classes['regular-text']}>
                The programme has been specifically packaged to enhance and continue to offer great learning exposures
                to the students and an opportunity to meet and interact with new friends: thus helping to prepare them
                for future challenges. Our student benefit tremendously from the programme, likewise the college
                accordingly.
            </p>

            <p className={classes['regular-text']}>
                Your child/student should now start enjoying the best of BRITISH EDUCATION. BEC – the International
                Education Consultants organizes the summer programme every year and both the old and new students are
                looking forward to a more exciting summer programme every year.
            </p>
        </Layout>
    );
};

export default EducationalTourPage;
