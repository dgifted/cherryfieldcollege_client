import React from 'react';

import Image from "next/image";
import {useRouter} from "next/router";

import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

import classes from './about-pages.module.css'
import Layout from "../../components/layouts/layout";
import ChairmanPic from '/public/assets/images/chairman.jpg';
import BannerImage from '/public/assets/images/school_view.png';
import {extractActiveLinkText} from "../../helpers/extract-links";
import {navMenu} from "../../misc/data";
import FooterImage from "../../public/assets/images/img_13-sm.png";
import useWindowSize from "../../hooks/use-window-size";

const ChairmanMessagePage = () => {
    const router = useRouter();
    const {width} = useWindowSize();

    return (
        <Layout
            activeLinkText={extractActiveLinkText(router.pathname, navMenu.links.about)}
            bannerImage={BannerImage}
            footerImage={FooterImage}
            navLinks={navMenu.links.about}
            parentLinkText={'About'}
        >
            <Container>
                <Row>
                    <Col
                        sm={{span: 12, order: 2}}
                        md={{span: 3, order: 1}}
                        className={'mb-4 text-center mb-sm-0 text-sm-left'}
                    >
                        <Image
                            alt={'Chairman'}
                            src={ChairmanPic}
                            width={`${width < 500 ? 380 : 240}`}
                            height={`${width < 500 ? 400 : 320}`}
                        />
                    </Col>

                    <Col sm={{span: 12, order: 1}} md={{span: 9, order: 2}}>
                        <p className={classes['intro-text']}>
                            Welcome to Cherryfield College Jikwoyi and its new Website. The College is honoured by you
                            for taking time to visit our site. It is our desire that the link will give you the
                            opportunity to have a glimpse of our serene learning environment with world class
                            infrastructure as well as academic excellence which is found in both our Secondary school
                            and the Nursery and Primary school.
                        </p>

                        <p className={classes['regular-text']}>
                            Our vision is to make Cherryfield College Jikwoyi the leader in secondary education in
                            Nigeria and this is aptly encapsulated in our college maxim ” PREPARING CHILDREN FOR
                            GREATNESS”. To some sceptics, this was a tall dream but events from inception of the college
                            in 2004 to date have been vindicated as contained in our academic record on the website.
                            Cherryfield College provides our students with world class educational facilities to enable
                            them have a sound and solid educational foundation while at the same time developing the
                            total child preparatory to the university education.
                        </p>

                        <p className={classes['regular-text']}>
                            It is our goal that the students can contribute to the mission by having the skills,
                            qualification to compete with and most importantly work with the best with an irrepressible
                            mind that allows the students to thrive under pressure and of inculcating high moral and of
                            ethic values will prepare them for a life of service and commitment.
                        </p>

                        <p className={classes['regular-text']}>
                            We believe that Cherryfield College offers a unique educational experience for all students
                            by providing a hybrid of the British and Nigerian curricular to provide a truly
                            international community. We also encourage international mindedness by exposing and
                            preparing our students to sit for such examinations as IGCSE, IELTS, TOEPL PSAT, SAT and
                            DELF (French).
                        </p>

                        <p className={classes['regular-text']}>
                            Cherryfield College provide a College of experience and richness of diverse cultures for the
                            students. We endorse enthusiasm for life and learning environment in a manner that allows
                            every student to excel and be valued in their sphere of influence. Our aim is to make future
                            leaders of these students such as to have self -confidence to meet the challenge of the
                            future, the humility to knowledge, the success of others, the compassion to support others
                            who encounter difficulties and the maturity to take responsibility for their own action.
                        </p>

                        <p className={classes['regular-text']}>
                            We say thank you for taking out time to visit our website and looking forward to your visit
                            to the school.
                        </p>
                    </Col>
                </Row>
            </Container>
        </Layout>
    );
};

export default ChairmanMessagePage;
