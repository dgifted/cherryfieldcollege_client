import React from 'react';

import Image from "next/image";
import {useRouter} from "next/router";

import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

import classes from './about-pages.module.css';
import BannerImage from '/public/assets/images/school_view.png';
import PrincipalPic from '/public/assets/images/principals-desk.jpg';
import Layout from "../../components/layouts/layout";
import {extractActiveLinkText} from "../../helpers/extract-links";
import {navMenu} from "../../misc/data";
import FooterImage from "../../public/assets/images/img_13-sm.png";
import useWindowSize from "../../hooks/use-window-size";

const PrincipalDeskPage = () => {
    const router = useRouter();
    const {width} = useWindowSize();

    return (
        <Layout
            activeLinkText={extractActiveLinkText(router.pathname, navMenu.links.about)}
            bannerImage={BannerImage}
            footerImage={FooterImage}
            navLinks={navMenu.links.about}
            parentLinkText={'About'}
        >
            <Container>
                <Row>
                    <Col
                        sm={{span: 12, order: 2}}
                        md={{span: 3, order: 1}}
                        className={'mb-4 text-center mb-sm-0 text-sm-left'}
                    >
                        <Image
                            alt={''}
                            src={PrincipalPic}
                            height={`${width < 500 ? 400 : 320}`}
                            width={`${width < 500 ? 380 : 240}`}
                        />
                    </Col>

                    <Col sm={{span: 12, order: 1}} md={{span: 9, order: 2}}>
                        <p className={classes['intro-text']}>
                            In today’s world, information and technology is developing at such speed that one needs to
                            constantly upgrade one’s knowledge in order to keep up.
                        </p>

                        <p className={classes['regular-text']}>
                            Any slowing of the pace will result in being left behind and turn your knowledge into relic.
                            For that reason, in Cherryfield College we constantly upgrade teaching methods, using modern
                            aids and technology and instilling same in our students to keep them at par with their
                            contemporaries all over the world. We desire that our children be well equipped to compete
                            with their peers from any parts of the globe. They are encouraged to think independently yet
                            guided by capable hands to channel their energy towards the right goals. That is why we give
                            our students the possibility to sit for all international examinations which enables them
                            gain admission into top universities the world over.
                        </p>

                        <p className={classes['regular-text']}>
                            We endeavor to groom future leaders of this country, that is why we insist on discipline in
                            areas of good conduct, punctuality, decent dressing and hard work. The children are educated
                            in a beautiful and conducive environment, fed with balanced and nourishing diet and
                            encouraged to develop harmoniously both physically and mentally. In the six years they spend
                            with us we try to instill virtues which make them stand out wherever they find themselves in
                            the future.
                        </p>

                        <p className={classes['regular-text']}>
                            We are not yet perfect, but we are always striving towards that!
                        </p>

                        <p className={classes['regular-text']}>
                            <b>Mrs. Olga Igbo –  Principal Cherryfield College</b>
                        </p>
                    </Col>
                </Row>
            </Container>
        </Layout>
    );
};

export default PrincipalDeskPage;
