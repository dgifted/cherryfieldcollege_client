import React from 'react';

import {useRouter} from "next/router";

import classes from './about-pages.module.css';
import BannerImage from '/public/assets/images/side_view_of_school.png';
import Layout from "../../components/layouts/layout";
import {extractActiveLinkText} from "../../helpers/extract-links";
import {navMenu} from "../../misc/data";
import FooterImage from "../../public/assets/images/img_13-sm.png";

const PoliciesPage = () => {
    const router = useRouter();

    return (
        <Layout
            activeLinkText={extractActiveLinkText(router.pathname, navMenu.links.about)}
            bannerImage={BannerImage}
            footerImage={FooterImage}
            navLinks={navMenu.links.about}
            parentLinkText={'About'}
        >
            <h4 className={classes['body-sub-heading']}>ADMISSION POLICY</h4>
            <ul className={classes['list']}>
                <li>Vision: to make CFC one of the leading schools in Abuja and Nigeria.</li>
                <li>Mission: to promote students’ high academic achievements , moral, personal and social wellbeing, to
                    support staff professional development and their career satisfaction; and to constructively engage
                    with the local and international community
                </li>
                <li>No admittance to new students into JSS 3, SS 2 and SS 3,</li>
            </ul>

            <h4 className={classes['body-sub-heading']}>ACADEMIC POLICY</h4>
            <ul className={classes['list']}>
                <li>In Internal examinations, students must get an average score of 60% and must pass English and
                    Mathematics to be promoted to the next class or repeat the same class.
                </li>
                <li>SS 1 students must get six credits including English and Mathematics in order to go on into SS 2.
                </li>
                <li>SS 2 students must get six credits in four (4) core subjects: English, Mathematics, Civic Education
                    and Entrepreneurial Studies in order to go on into SS 3.
                </li>
                <li>Parents will be allowed to visit only if there are genuine medical reasons. Otherwise they can come
                    on Open/Visiting Day(s). Renewal of Passport/Visa appointment is on special permission by school
                    authority.
                </li>
            </ul>

            <h4 className={classes['body-sub-heading']}>SOCIAL POLICY</h4>
            <ul className={classes['list']}>
                <li>CFC will provide school uniform which must be worn at all times.</li>
                <li>No provisions are to be brought into CFC for students by their families.</li>
                <li>Everybody should be given equal treatment and consideration irrespective of his/her religious
                    leaning.
                </li>
                <li>Every person should be given equal treatment and consideration, no matter their disability so long
                    as it’s within the limitations of CFC facilities.
                </li>
                <li>Every member of staff should be given equal treatment and consideration no matter what their age
                    brackets are.
                </li>
                <li>Every student should be given equal treatment and consideration no matter what their age brackets
                    are.
                </li>
                <li>Bribery for favour will not be tolerated. Arrangements for private lessons by parents are also
                    prohibited.
                </li>
                <li>The 3Fs: Students should be given firm, friendly and fair treatment.</li>
                <li>The 3 Rs are about the rights, requirements and responsibilities being ensured.</li>
                <li>The 4 Ds: In their studying, students are expected to exhibit desire, diligence, discipline and
                    determination.
                </li>
                <li>The 5 Ps: all CFC staff, students and their families are expected to follow – polite, proper,
                    procedure, process and protocol.
                </li>
                <li>Students are not allowed to register for external WASSCE/NECO/GCE.</li>
            </ul>
        </Layout>
    );
};

export default PoliciesPage;
