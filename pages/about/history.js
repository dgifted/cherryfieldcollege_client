import React from 'react';

import {useRouter} from "next/router";

import {extractActiveLinkText} from "../../helpers/extract-links";
import {navMenu} from "../../misc/data";
import BannerImage from "/public/assets/images/school-front.png";
import FooterImage from "/public/assets/images/img_13-sm.png";
import Layout from "../../components/layouts/layout";

import classes from './about-pages.module.css';
import MoveToTop from "../../components/move-to-top";

const AboutHomePage = () => {
    const router = useRouter();
    console.log('router ', router.pathname);

    return (
        <>
            <Layout
                activeLinkText={extractActiveLinkText(router.pathname, navMenu.links.about)}
                bannerImage={BannerImage}
                footerImage={FooterImage}
                navLinks={navMenu.links.about}
                parentLinkText={'About'}
            >

                <p className={classes['regular-text']}>TIn September, 2004, the gate of this citadel of learning was
                    flung open to ninety-six (96) students drawn from different parts of the country. At the gate of the
                    welcome team was the pioneer British principal, Mr. Robert Parkin. Another British principal came on
                    board in 2006 by name, Mr. Barry Tipton. The current principal, Mrs. Olga Igbo (BA in Linguistics
                    (1977), MA in Linguistics (1979) and Ph D in Teaching of Second Languages (2000), is today piloting
                    the affair of the college. The Principal is being assisted by two vice principals, Mrs. R. O. Idibia
                    (Vice Principal, Administration) and Mrs. O. E. Onyekwelu, (Vice Principal, Academics). The college
                    started in 2004 with a student population of 96 and 76 academic and non-academic staff. Since then
                    the college has witnessed an astronomical growth in the in the students’ population and staff
                    strength. Now the college has a population of 504 students. The growth in students’ population as
                    usual necessitated an equivalent growth in staff population. Today, the college has over 250
                    experienced and qualified staff. Out of this, 71 are teaching staff. Board of Directors/Board of
                    Governors: The Board of Directors is made up of Hon. Lt. Col. Akor JAC (rtd), Engr. John Agbara and
                    Mr. Abah Agbara while the Board of Governors includes Prof. P. B. Onaji, Prof. I.A.O. Ujah mni, Mr.
                    Raphael Odoh, Mr. John-Mary Idoko and Barr. B. M. O. Enejoh.teachers can
                    collaborate with parents in their most important role of educating their children.</p>
            </Layout>

            <MoveToTop/>
        </>
    );
}

export default AboutHomePage;
